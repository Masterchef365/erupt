#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const NV_SAMPLE_MASK_OVERRIDE_COVERAGE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const NV_SAMPLE_MASK_OVERRIDE_COVERAGE_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_NV_sample_mask_override_coverage");
