#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_SWAPCHAIN_COLOR_SPACE_SPEC_VERSION: u32 = 4;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_SWAPCHAIN_COLOR_SPACE_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_EXT_swapchain_colorspace");
