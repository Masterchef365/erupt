#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const AMD_GPU_SHADER_HALF_FLOAT_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const AMD_GPU_SHADER_HALF_FLOAT_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_AMD_gpu_shader_half_float");
