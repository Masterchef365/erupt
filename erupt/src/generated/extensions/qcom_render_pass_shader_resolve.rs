#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const QCOM_RENDER_PASS_SHADER_RESOLVE_SPEC_VERSION: u32 = 4;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const QCOM_RENDER_PASS_SHADER_RESOLVE_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_QCOM_render_pass_shader_resolve");
