#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_8BIT_STORAGE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_8BIT_STORAGE_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_8bit_storage");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevice8BitStorageFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDevice8BitStorageFeaturesKHR = crate::vk1_2::PhysicalDevice8BitStorageFeatures;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevice8BitStorageFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDevice8BitStorageFeaturesKHRBuilder<'a> =
    crate::vk1_2::PhysicalDevice8BitStorageFeaturesBuilder<'a>;
