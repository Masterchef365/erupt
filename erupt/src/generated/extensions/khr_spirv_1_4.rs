#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SPIRV_1_4_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SPIRV_1_4_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_spirv_1_4");
