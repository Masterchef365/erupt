#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_IMAGELESS_FRAMEBUFFER_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_IMAGELESS_FRAMEBUFFER_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_imageless_framebuffer");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceImagelessFramebufferFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceImagelessFramebufferFeaturesKHR =
    crate::vk1_2::PhysicalDeviceImagelessFramebufferFeatures;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceImagelessFramebufferFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceImagelessFramebufferFeaturesKHRBuilder<'a> =
    crate::vk1_2::PhysicalDeviceImagelessFramebufferFeaturesBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFramebufferAttachmentsCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type FramebufferAttachmentsCreateInfoKHR = crate::vk1_2::FramebufferAttachmentsCreateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFramebufferAttachmentsCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type FramebufferAttachmentsCreateInfoKHRBuilder<'a> =
    crate::vk1_2::FramebufferAttachmentsCreateInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFramebufferAttachmentImageInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type FramebufferAttachmentImageInfoKHR = crate::vk1_2::FramebufferAttachmentImageInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFramebufferAttachmentImageInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type FramebufferAttachmentImageInfoKHRBuilder<'a> =
    crate::vk1_2::FramebufferAttachmentImageInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassAttachmentBeginInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type RenderPassAttachmentBeginInfoKHR = crate::vk1_2::RenderPassAttachmentBeginInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassAttachmentBeginInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type RenderPassAttachmentBeginInfoKHRBuilder<'a> =
    crate::vk1_2::RenderPassAttachmentBeginInfoBuilder<'a>;
