#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SHADER_FLOAT16_INT8_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SHADER_FLOAT16_INT8_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_shader_float16_int8");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderFloat16Int8FeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceShaderFloat16Int8FeaturesKHR =
    crate::vk1_2::PhysicalDeviceShaderFloat16Int8Features;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderFloat16Int8FeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceShaderFloat16Int8FeaturesKHRBuilder<'a> =
    crate::vk1_2::PhysicalDeviceShaderFloat16Int8FeaturesBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFloat16Int8FeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceFloat16Int8FeaturesKHR =
    crate::vk1_2::PhysicalDeviceShaderFloat16Int8Features;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFloat16Int8FeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceFloat16Int8FeaturesKHRBuilder<'a> =
    crate::vk1_2::PhysicalDeviceShaderFloat16Int8FeaturesBuilder<'a>;
