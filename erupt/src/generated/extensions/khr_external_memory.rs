#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const QUEUE_FAMILY_EXTERNAL_KHR: u32 = 4294967294;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_EXTERNAL_MEMORY_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_EXTERNAL_MEMORY_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_external_memory");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryImageCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ExternalMemoryImageCreateInfoKHR = crate::vk1_1::ExternalMemoryImageCreateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryImageCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ExternalMemoryImageCreateInfoKHRBuilder<'a> =
    crate::vk1_1::ExternalMemoryImageCreateInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryBufferCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ExternalMemoryBufferCreateInfoKHR = crate::vk1_1::ExternalMemoryBufferCreateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryBufferCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ExternalMemoryBufferCreateInfoKHRBuilder<'a> =
    crate::vk1_1::ExternalMemoryBufferCreateInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportMemoryAllocateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ExportMemoryAllocateInfoKHR = crate::vk1_1::ExportMemoryAllocateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportMemoryAllocateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ExportMemoryAllocateInfoKHRBuilder<'a> = crate::vk1_1::ExportMemoryAllocateInfoBuilder<'a>;
