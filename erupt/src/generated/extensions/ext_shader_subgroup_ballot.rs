#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_SHADER_SUBGROUP_BALLOT_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_SHADER_SUBGROUP_BALLOT_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_EXT_shader_subgroup_ballot");
