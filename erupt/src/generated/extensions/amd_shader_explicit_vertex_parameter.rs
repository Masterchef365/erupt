#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const AMD_SHADER_EXPLICIT_VERTEX_PARAMETER_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const AMD_SHADER_EXPLICIT_VERTEX_PARAMETER_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_AMD_shader_explicit_vertex_parameter");
