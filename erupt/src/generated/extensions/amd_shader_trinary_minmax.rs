#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const AMD_SHADER_TRINARY_MINMAX_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const AMD_SHADER_TRINARY_MINMAX_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_AMD_shader_trinary_minmax");
