#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_get_physical_device_properties2");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_PHYSICAL_DEVICE_FEATURES2_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkGetPhysicalDeviceFeatures2KHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_PHYSICAL_DEVICE_PROPERTIES2_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkGetPhysicalDeviceProperties2KHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_PHYSICAL_DEVICE_FORMAT_PROPERTIES2_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkGetPhysicalDeviceFormatProperties2KHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_PHYSICAL_DEVICE_IMAGE_FORMAT_PROPERTIES2_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkGetPhysicalDeviceImageFormatProperties2KHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_PHYSICAL_DEVICE_QUEUE_FAMILY_PROPERTIES2_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkGetPhysicalDeviceQueueFamilyProperties2KHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_PHYSICAL_DEVICE_MEMORY_PROPERTIES2_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkGetPhysicalDeviceMemoryProperties2KHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_PROPERTIES2_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkGetPhysicalDeviceSparseImageFormatProperties2KHR");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFeatures2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceFeatures2KHR = crate::vk1_1::PhysicalDeviceFeatures2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFeatures2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceFeatures2KHRBuilder<'a> = crate::vk1_1::PhysicalDeviceFeatures2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceProperties2KHR = crate::vk1_1::PhysicalDeviceProperties2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceProperties2KHRBuilder<'a> =
    crate::vk1_1::PhysicalDeviceProperties2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFormatProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type FormatProperties2KHR = crate::vk1_1::FormatProperties2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFormatProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type FormatProperties2KHRBuilder<'a> = crate::vk1_1::FormatProperties2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageFormatProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ImageFormatProperties2KHR = crate::vk1_1::ImageFormatProperties2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageFormatProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ImageFormatProperties2KHRBuilder<'a> = crate::vk1_1::ImageFormatProperties2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceImageFormatInfo2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceImageFormatInfo2KHR = crate::vk1_1::PhysicalDeviceImageFormatInfo2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceImageFormatInfo2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceImageFormatInfo2KHRBuilder<'a> =
    crate::vk1_1::PhysicalDeviceImageFormatInfo2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueueFamilyProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type QueueFamilyProperties2KHR = crate::vk1_1::QueueFamilyProperties2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueueFamilyProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type QueueFamilyProperties2KHRBuilder<'a> = crate::vk1_1::QueueFamilyProperties2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMemoryProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceMemoryProperties2KHR = crate::vk1_1::PhysicalDeviceMemoryProperties2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMemoryProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceMemoryProperties2KHRBuilder<'a> =
    crate::vk1_1::PhysicalDeviceMemoryProperties2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSparseImageFormatProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SparseImageFormatProperties2KHR = crate::vk1_1::SparseImageFormatProperties2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSparseImageFormatProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SparseImageFormatProperties2KHRBuilder<'a> =
    crate::vk1_1::SparseImageFormatProperties2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceSparseImageFormatInfo2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceSparseImageFormatInfo2KHR =
    crate::vk1_1::PhysicalDeviceSparseImageFormatInfo2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceSparseImageFormatInfo2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceSparseImageFormatInfo2KHRBuilder<'a> =
    crate::vk1_1::PhysicalDeviceSparseImageFormatInfo2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceFeatures2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceFeatures2KHR = crate::vk1_1::PFN_vkGetPhysicalDeviceFeatures2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceProperties2KHR = crate::vk1_1::PFN_vkGetPhysicalDeviceProperties2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceFormatProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceFormatProperties2KHR =
    crate::vk1_1::PFN_vkGetPhysicalDeviceFormatProperties2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceImageFormatProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceImageFormatProperties2KHR =
    crate::vk1_1::PFN_vkGetPhysicalDeviceImageFormatProperties2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceQueueFamilyProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceQueueFamilyProperties2KHR =
    crate::vk1_1::PFN_vkGetPhysicalDeviceQueueFamilyProperties2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceMemoryProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceMemoryProperties2KHR =
    crate::vk1_1::PFN_vkGetPhysicalDeviceMemoryProperties2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSparseImageFormatProperties2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceSparseImageFormatProperties2KHR =
    crate::vk1_1::PFN_vkGetPhysicalDeviceSparseImageFormatProperties2;
#[doc = "Provided by [`extensions::khr_get_physical_device_properties2`](extensions/khr_get_physical_device_properties2/index.html)"]
impl crate::InstanceLoader {
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceFeatures2KHR.html) · Function"]
    pub unsafe fn get_physical_device_features2_khr(
        &self,
        physical_device: crate::vk1_0::PhysicalDevice,
        features: Option<crate::vk1_1::PhysicalDeviceFeatures2>,
    ) -> crate::vk1_1::PhysicalDeviceFeatures2 {
        let _function = self
            .get_physical_device_features2_khr
            .expect("`get_physical_device_features2_khr` is not loaded");
        let mut features = match features {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(physical_device as _, &mut features);
        features
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceProperties2KHR.html) · Function"]
    pub unsafe fn get_physical_device_properties2_khr(
        &self,
        physical_device: crate::vk1_0::PhysicalDevice,
        properties: Option<crate::vk1_1::PhysicalDeviceProperties2>,
    ) -> crate::vk1_1::PhysicalDeviceProperties2 {
        let _function = self
            .get_physical_device_properties2_khr
            .expect("`get_physical_device_properties2_khr` is not loaded");
        let mut properties = match properties {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(physical_device as _, &mut properties);
        properties
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceFormatProperties2KHR.html) · Function"]
    pub unsafe fn get_physical_device_format_properties2_khr(
        &self,
        physical_device: crate::vk1_0::PhysicalDevice,
        format: crate::vk1_0::Format,
        format_properties: Option<crate::vk1_1::FormatProperties2>,
    ) -> crate::vk1_1::FormatProperties2 {
        let _function = self
            .get_physical_device_format_properties2_khr
            .expect("`get_physical_device_format_properties2_khr` is not loaded");
        let mut format_properties = match format_properties {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(physical_device as _, format as _, &mut format_properties);
        format_properties
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceImageFormatProperties2KHR.html) · Function"]
    pub unsafe fn get_physical_device_image_format_properties2_khr(
        &self,
        physical_device: crate::vk1_0::PhysicalDevice,
        image_format_info: &crate::vk1_1::PhysicalDeviceImageFormatInfo2,
        image_format_properties: Option<crate::vk1_1::ImageFormatProperties2>,
    ) -> crate::utils::VulkanResult<crate::vk1_1::ImageFormatProperties2> {
        let _function = self
            .get_physical_device_image_format_properties2_khr
            .expect("`get_physical_device_image_format_properties2_khr` is not loaded");
        let mut image_format_properties = match image_format_properties {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(
            physical_device as _,
            image_format_info as _,
            &mut image_format_properties,
        );
        crate::utils::VulkanResult::new(_return, image_format_properties)
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceQueueFamilyProperties2KHR.html) · Function"]
    pub unsafe fn get_physical_device_queue_family_properties2_khr(
        &self,
        physical_device: crate::vk1_0::PhysicalDevice,
        queue_family_property_count: Option<u32>,
    ) -> Vec<crate::vk1_1::QueueFamilyProperties2> {
        let _function = self
            .get_physical_device_queue_family_properties2_khr
            .expect("`get_physical_device_queue_family_properties2_khr` is not loaded");
        let mut queue_family_property_count = match queue_family_property_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut queue_family_properties =
            vec![Default::default(); queue_family_property_count as _];
        let _return = _function(
            physical_device as _,
            &mut queue_family_property_count,
            queue_family_properties.as_mut_ptr(),
        );
        queue_family_properties
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceMemoryProperties2KHR.html) · Function"]
    pub unsafe fn get_physical_device_memory_properties2_khr(
        &self,
        physical_device: crate::vk1_0::PhysicalDevice,
        memory_properties: Option<crate::vk1_1::PhysicalDeviceMemoryProperties2>,
    ) -> crate::vk1_1::PhysicalDeviceMemoryProperties2 {
        let _function = self
            .get_physical_device_memory_properties2_khr
            .expect("`get_physical_device_memory_properties2_khr` is not loaded");
        let mut memory_properties = match memory_properties {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(physical_device as _, &mut memory_properties);
        memory_properties
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSparseImageFormatProperties2KHR.html) · Function"]
    pub unsafe fn get_physical_device_sparse_image_format_properties2_khr(
        &self,
        physical_device: crate::vk1_0::PhysicalDevice,
        format_info: &crate::vk1_1::PhysicalDeviceSparseImageFormatInfo2,
        property_count: Option<u32>,
    ) -> Vec<crate::vk1_1::SparseImageFormatProperties2> {
        let _function = self
            .get_physical_device_sparse_image_format_properties2_khr
            .expect("`get_physical_device_sparse_image_format_properties2_khr` is not loaded");
        let mut property_count = match property_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(
                    physical_device as _,
                    format_info as _,
                    &mut v,
                    std::ptr::null_mut(),
                );
                v
            }
        };
        let mut properties = vec![Default::default(); property_count as _];
        let _return = _function(
            physical_device as _,
            format_info as _,
            &mut property_count,
            properties.as_mut_ptr(),
        );
        properties
    }
}
