#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_16BIT_STORAGE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_16BIT_STORAGE_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_16bit_storage");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevice16BitStorageFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDevice16BitStorageFeaturesKHR = crate::vk1_1::PhysicalDevice16BitStorageFeatures;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevice16BitStorageFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDevice16BitStorageFeaturesKHRBuilder<'a> =
    crate::vk1_1::PhysicalDevice16BitStorageFeaturesBuilder<'a>;
