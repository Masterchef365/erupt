#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_CREATE_RENDERPASS_2_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_CREATE_RENDERPASS_2_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_create_renderpass2");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CREATE_RENDER_PASS2_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkCreateRenderPass2KHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CMD_BEGIN_RENDER_PASS2_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkCmdBeginRenderPass2KHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CMD_NEXT_SUBPASS2_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkCmdNextSubpass2KHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CMD_END_RENDER_PASS2_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkCmdEndRenderPass2KHR");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAttachmentDescription2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type AttachmentDescription2KHR = crate::vk1_2::AttachmentDescription2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAttachmentDescription2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type AttachmentDescription2KHRBuilder<'a> = crate::vk1_2::AttachmentDescription2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAttachmentReference2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type AttachmentReference2KHR = crate::vk1_2::AttachmentReference2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAttachmentReference2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type AttachmentReference2KHRBuilder<'a> = crate::vk1_2::AttachmentReference2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassDescription2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SubpassDescription2KHR = crate::vk1_2::SubpassDescription2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassDescription2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SubpassDescription2KHRBuilder<'a> = crate::vk1_2::SubpassDescription2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassDependency2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SubpassDependency2KHR = crate::vk1_2::SubpassDependency2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassDependency2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SubpassDependency2KHRBuilder<'a> = crate::vk1_2::SubpassDependency2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassCreateInfo2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type RenderPassCreateInfo2KHR = crate::vk1_2::RenderPassCreateInfo2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassCreateInfo2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type RenderPassCreateInfo2KHRBuilder<'a> = crate::vk1_2::RenderPassCreateInfo2Builder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassBeginInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SubpassBeginInfoKHR = crate::vk1_2::SubpassBeginInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassBeginInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SubpassBeginInfoKHRBuilder<'a> = crate::vk1_2::SubpassBeginInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassEndInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SubpassEndInfoKHR = crate::vk1_2::SubpassEndInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassEndInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SubpassEndInfoKHRBuilder<'a> = crate::vk1_2::SubpassEndInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateRenderPass2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateRenderPass2KHR = crate::vk1_2::PFN_vkCreateRenderPass2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBeginRenderPass2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdBeginRenderPass2KHR = crate::vk1_2::PFN_vkCmdBeginRenderPass2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdNextSubpass2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdNextSubpass2KHR = crate::vk1_2::PFN_vkCmdNextSubpass2;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdEndRenderPass2KHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdEndRenderPass2KHR = crate::vk1_2::PFN_vkCmdEndRenderPass2;
#[doc = "Provided by [`extensions::khr_create_renderpass2`](extensions/khr_create_renderpass2/index.html)"]
impl crate::DeviceLoader {
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateRenderPass2KHR.html) · Function"]
    pub unsafe fn create_render_pass2_khr(
        &self,
        create_info: &crate::vk1_2::RenderPassCreateInfo2,
        allocator: Option<&crate::vk1_0::AllocationCallbacks>,
        render_pass: Option<crate::vk1_0::RenderPass>,
    ) -> crate::utils::VulkanResult<crate::vk1_0::RenderPass> {
        let _function = self
            .create_render_pass2_khr
            .expect("`create_render_pass2_khr` is not loaded");
        let mut render_pass = match render_pass {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut render_pass,
        );
        crate::utils::VulkanResult::new(_return, render_pass)
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBeginRenderPass2KHR.html) · Function"]
    pub unsafe fn cmd_begin_render_pass2_khr(
        &self,
        command_buffer: crate::vk1_0::CommandBuffer,
        render_pass_begin: &crate::vk1_0::RenderPassBeginInfo,
        subpass_begin_info: &crate::vk1_2::SubpassBeginInfo,
    ) -> () {
        let _function = self
            .cmd_begin_render_pass2_khr
            .expect("`cmd_begin_render_pass2_khr` is not loaded");
        let _return = _function(
            command_buffer as _,
            render_pass_begin as _,
            subpass_begin_info as _,
        );
        ()
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdNextSubpass2KHR.html) · Function"]
    pub unsafe fn cmd_next_subpass2_khr(
        &self,
        command_buffer: crate::vk1_0::CommandBuffer,
        subpass_begin_info: &crate::vk1_2::SubpassBeginInfo,
        subpass_end_info: &crate::vk1_2::SubpassEndInfo,
    ) -> () {
        let _function = self
            .cmd_next_subpass2_khr
            .expect("`cmd_next_subpass2_khr` is not loaded");
        let _return = _function(
            command_buffer as _,
            subpass_begin_info as _,
            subpass_end_info as _,
        );
        ()
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdEndRenderPass2KHR.html) · Function"]
    pub unsafe fn cmd_end_render_pass2_khr(
        &self,
        command_buffer: crate::vk1_0::CommandBuffer,
        subpass_end_info: &crate::vk1_2::SubpassEndInfo,
    ) -> () {
        let _function = self
            .cmd_end_render_pass2_khr
            .expect("`cmd_end_render_pass2_khr` is not loaded");
        let _return = _function(command_buffer as _, subpass_end_info as _);
        ()
    }
}
