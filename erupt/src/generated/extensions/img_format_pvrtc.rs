#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const IMG_FORMAT_PVRTC_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const IMG_FORMAT_PVRTC_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_IMG_format_pvrtc");
