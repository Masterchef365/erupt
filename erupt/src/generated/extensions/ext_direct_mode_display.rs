#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_DIRECT_MODE_DISPLAY_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_DIRECT_MODE_DISPLAY_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_EXT_direct_mode_display");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_RELEASE_DISPLAY_EXT: *const std::os::raw::c_char = crate::cstr!("vkReleaseDisplayEXT");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkReleaseDisplayEXT.html) · Function"]
#[allow(non_camel_case_types)]
pub type PFN_vkReleaseDisplayEXT = unsafe extern "system" fn(
    physical_device: crate::vk1_0::PhysicalDevice,
    display: crate::extensions::khr_display::DisplayKHR,
) -> crate::vk1_0::Result;
#[doc = "Provided by [`extensions::ext_direct_mode_display`](extensions/ext_direct_mode_display/index.html)"]
impl crate::InstanceLoader {
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkReleaseDisplayEXT.html) · Function"]
    pub unsafe fn release_display_ext(
        &self,
        physical_device: crate::vk1_0::PhysicalDevice,
        display: crate::extensions::khr_display::DisplayKHR,
    ) -> crate::utils::VulkanResult<()> {
        let _function = self
            .release_display_ext
            .expect("`release_display_ext` is not loaded");
        let _return = _function(physical_device as _, display as _);
        crate::utils::VulkanResult::new(_return, ())
    }
}
