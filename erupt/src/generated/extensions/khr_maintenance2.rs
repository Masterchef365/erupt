#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_MAINTENANCE2_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_MAINTENANCE2_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_maintenance2");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPointClippingBehaviorKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PointClippingBehaviorKHR = crate::vk1_1::PointClippingBehavior;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkTessellationDomainOriginKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type TessellationDomainOriginKHR = crate::vk1_1::TessellationDomainOrigin;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkInputAttachmentAspectReferenceKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type InputAttachmentAspectReferenceKHR = crate::vk1_1::InputAttachmentAspectReference;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkInputAttachmentAspectReferenceKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type InputAttachmentAspectReferenceKHRBuilder<'a> =
    crate::vk1_1::InputAttachmentAspectReferenceBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassInputAttachmentAspectCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type RenderPassInputAttachmentAspectCreateInfoKHR =
    crate::vk1_1::RenderPassInputAttachmentAspectCreateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassInputAttachmentAspectCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type RenderPassInputAttachmentAspectCreateInfoKHRBuilder<'a> =
    crate::vk1_1::RenderPassInputAttachmentAspectCreateInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePointClippingPropertiesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDevicePointClippingPropertiesKHR =
    crate::vk1_1::PhysicalDevicePointClippingProperties;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePointClippingPropertiesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDevicePointClippingPropertiesKHRBuilder<'a> =
    crate::vk1_1::PhysicalDevicePointClippingPropertiesBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageViewUsageCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ImageViewUsageCreateInfoKHR = crate::vk1_1::ImageViewUsageCreateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageViewUsageCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ImageViewUsageCreateInfoKHRBuilder<'a> = crate::vk1_1::ImageViewUsageCreateInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineTessellationDomainOriginStateCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PipelineTessellationDomainOriginStateCreateInfoKHR =
    crate::vk1_1::PipelineTessellationDomainOriginStateCreateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineTessellationDomainOriginStateCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PipelineTessellationDomainOriginStateCreateInfoKHRBuilder<'a> =
    crate::vk1_1::PipelineTessellationDomainOriginStateCreateInfoBuilder<'a>;
