#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SWAPCHAIN_MUTABLE_FORMAT_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SWAPCHAIN_MUTABLE_FORMAT_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_swapchain_mutable_format");
