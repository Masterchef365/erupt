#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_SCALAR_BLOCK_LAYOUT_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_SCALAR_BLOCK_LAYOUT_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_EXT_scalar_block_layout");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceScalarBlockLayoutFeaturesEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceScalarBlockLayoutFeaturesEXT =
    crate::vk1_2::PhysicalDeviceScalarBlockLayoutFeatures;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceScalarBlockLayoutFeaturesEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceScalarBlockLayoutFeaturesEXTBuilder<'a> =
    crate::vk1_2::PhysicalDeviceScalarBlockLayoutFeaturesBuilder<'a>;
