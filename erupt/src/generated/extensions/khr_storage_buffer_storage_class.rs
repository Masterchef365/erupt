#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_STORAGE_BUFFER_STORAGE_CLASS_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_STORAGE_BUFFER_STORAGE_CLASS_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_storage_buffer_storage_class");
