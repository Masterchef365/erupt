#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SHADER_DRAW_PARAMETERS_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SHADER_DRAW_PARAMETERS_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_shader_draw_parameters");
