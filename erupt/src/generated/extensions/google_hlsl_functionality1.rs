#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const GOOGLE_HLSL_FUNCTIONALITY1_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const GOOGLE_HLSL_FUNCTIONALITY1_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_GOOGLE_hlsl_functionality1");
