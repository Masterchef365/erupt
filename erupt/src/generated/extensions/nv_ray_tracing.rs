#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const SHADER_UNUSED_NV: u32 = 4294967295;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const NV_RAY_TRACING_SPEC_VERSION: u32 = 3;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const NV_RAY_TRACING_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_NV_ray_tracing");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_COMPILE_DEFERRED_NV: *const std::os::raw::c_char = crate::cstr!("vkCompileDeferredNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CREATE_ACCELERATION_STRUCTURE_NV: *const std::os::raw::c_char =
    crate::cstr!("vkCreateAccelerationStructureNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_NV: *const std::os::raw::c_char =
    crate::cstr!("vkGetAccelerationStructureMemoryRequirementsNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CMD_COPY_ACCELERATION_STRUCTURE_NV: *const std::os::raw::c_char =
    crate::cstr!("vkCmdCopyAccelerationStructureNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CMD_BUILD_ACCELERATION_STRUCTURE_NV: *const std::os::raw::c_char =
    crate::cstr!("vkCmdBuildAccelerationStructureNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CMD_TRACE_RAYS_NV: *const std::os::raw::c_char = crate::cstr!("vkCmdTraceRaysNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_ACCELERATION_STRUCTURE_HANDLE_NV: *const std::os::raw::c_char =
    crate::cstr!("vkGetAccelerationStructureHandleNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CREATE_RAY_TRACING_PIPELINES_NV: *const std::os::raw::c_char =
    crate::cstr!("vkCreateRayTracingPipelinesNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_DESTROY_ACCELERATION_STRUCTURE_NV: *const std::os::raw::c_char =
    crate::cstr!("vkDestroyAccelerationStructureNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_BIND_ACCELERATION_STRUCTURE_MEMORY_NV: *const std::os::raw::c_char =
    crate::cstr!("vkBindAccelerationStructureMemoryNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CMD_WRITE_ACCELERATION_STRUCTURES_PROPERTIES_NV: *const std::os::raw::c_char =
    crate::cstr!("vkCmdWriteAccelerationStructuresPropertiesNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_RAY_TRACING_SHADER_GROUP_HANDLES_NV: *const std::os::raw::c_char =
    crate::cstr!("vkGetRayTracingShaderGroupHandlesNV");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryFlagsNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type GeometryFlagsNV = crate::extensions::khr_ray_tracing::GeometryFlagsKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryInstanceFlagsNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type GeometryInstanceFlagsNV = crate::extensions::khr_ray_tracing::GeometryInstanceFlagsKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBuildAccelerationStructureFlagsNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BuildAccelerationStructureFlagsNV =
    crate::extensions::khr_ray_tracing::BuildAccelerationStructureFlagsKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type AccelerationStructureNV = crate::extensions::khr_ray_tracing::AccelerationStructureKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryFlagBitsNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type GeometryFlagBitsNV = crate::extensions::khr_ray_tracing::GeometryFlagBitsKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryInstanceFlagBitsNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type GeometryInstanceFlagBitsNV =
    crate::extensions::khr_ray_tracing::GeometryInstanceFlagBitsKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBuildAccelerationStructureFlagBitsNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BuildAccelerationStructureFlagBitsNV =
    crate::extensions::khr_ray_tracing::BuildAccelerationStructureFlagBitsKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCopyAccelerationStructureModeNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type CopyAccelerationStructureModeNV =
    crate::extensions::khr_ray_tracing::CopyAccelerationStructureModeKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureTypeNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type AccelerationStructureTypeNV =
    crate::extensions::khr_ray_tracing::AccelerationStructureTypeKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryTypeNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type GeometryTypeNV = crate::extensions::khr_ray_tracing::GeometryTypeKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRayTracingShaderGroupTypeNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type RayTracingShaderGroupTypeNV =
    crate::extensions::khr_ray_tracing::RayTracingShaderGroupTypeKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMemoryRequirementsTypeNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type AccelerationStructureMemoryRequirementsTypeNV =
    crate::extensions::khr_ray_tracing::AccelerationStructureMemoryRequirementsTypeKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindAccelerationStructureMemoryInfoNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BindAccelerationStructureMemoryInfoNV =
    crate::extensions::khr_ray_tracing::BindAccelerationStructureMemoryInfoKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindAccelerationStructureMemoryInfoNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BindAccelerationStructureMemoryInfoNVBuilder<'a> =
    crate::extensions::khr_ray_tracing::BindAccelerationStructureMemoryInfoKHRBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkWriteDescriptorSetAccelerationStructureNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type WriteDescriptorSetAccelerationStructureNV =
    crate::extensions::khr_ray_tracing::WriteDescriptorSetAccelerationStructureKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkWriteDescriptorSetAccelerationStructureNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type WriteDescriptorSetAccelerationStructureNVBuilder<'a> =
    crate::extensions::khr_ray_tracing::WriteDescriptorSetAccelerationStructureKHRBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAabbPositionsNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type AabbPositionsNV = crate::extensions::khr_ray_tracing::AabbPositionsKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAabbPositionsNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type AabbPositionsNVBuilder<'a> =
    crate::extensions::khr_ray_tracing::AabbPositionsKHRBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkTransformMatrixNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type TransformMatrixNV = crate::extensions::khr_ray_tracing::TransformMatrixKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkTransformMatrixNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type TransformMatrixNVBuilder<'a> =
    crate::extensions::khr_ray_tracing::TransformMatrixKHRBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureInstanceNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type AccelerationStructureInstanceNV =
    crate::extensions::khr_ray_tracing::AccelerationStructureInstanceKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureInstanceNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type AccelerationStructureInstanceNVBuilder<'a> =
    crate::extensions::khr_ray_tracing::AccelerationStructureInstanceKHRBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyAccelerationStructureNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkDestroyAccelerationStructureNV =
    crate::extensions::khr_ray_tracing::PFN_vkDestroyAccelerationStructureKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkBindAccelerationStructureMemoryNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkBindAccelerationStructureMemoryNV =
    crate::extensions::khr_ray_tracing::PFN_vkBindAccelerationStructureMemoryKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdWriteAccelerationStructuresPropertiesNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdWriteAccelerationStructuresPropertiesNV =
    crate::extensions::khr_ray_tracing::PFN_vkCmdWriteAccelerationStructuresPropertiesKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetRayTracingShaderGroupHandlesNV.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetRayTracingShaderGroupHandlesNV =
    crate::extensions::khr_ray_tracing::PFN_vkGetRayTracingShaderGroupHandlesKHR;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCompileDeferredNV.html) · Function"]
#[allow(non_camel_case_types)]
pub type PFN_vkCompileDeferredNV = unsafe extern "system" fn(
    device: crate::vk1_0::Device,
    pipeline: crate::vk1_0::Pipeline,
    shader: u32,
) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateAccelerationStructureNV.html) · Function"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateAccelerationStructureNV = unsafe extern "system" fn(
    device: crate::vk1_0::Device,
    p_create_info: *const crate::extensions::nv_ray_tracing::AccelerationStructureCreateInfoNV,
    p_allocator: *const crate::vk1_0::AllocationCallbacks,
    p_acceleration_structure: *mut crate::extensions::nv_ray_tracing::AccelerationStructureNV,
) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetAccelerationStructureMemoryRequirementsNV.html) · Function"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetAccelerationStructureMemoryRequirementsNV = unsafe extern "system" fn ( device : crate :: vk1_0 :: Device , p_info : * const crate :: extensions :: nv_ray_tracing :: AccelerationStructureMemoryRequirementsInfoNV , p_memory_requirements : * mut crate :: extensions :: khr_get_memory_requirements2 :: MemoryRequirements2KHR ) -> std :: ffi :: c_void ;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdCopyAccelerationStructureNV.html) · Function"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdCopyAccelerationStructureNV = unsafe extern "system" fn(
    command_buffer: crate::vk1_0::CommandBuffer,
    dst: crate::extensions::khr_ray_tracing::AccelerationStructureKHR,
    src: crate::extensions::khr_ray_tracing::AccelerationStructureKHR,
    mode: crate::extensions::khr_ray_tracing::CopyAccelerationStructureModeKHR,
) -> std::ffi::c_void;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBuildAccelerationStructureNV.html) · Function"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdBuildAccelerationStructureNV = unsafe extern "system" fn(
    command_buffer: crate::vk1_0::CommandBuffer,
    p_info: *const crate::extensions::nv_ray_tracing::AccelerationStructureInfoNV,
    instance_data: crate::vk1_0::Buffer,
    instance_offset: crate::vk1_0::DeviceSize,
    update: crate::vk1_0::Bool32,
    dst: crate::extensions::khr_ray_tracing::AccelerationStructureKHR,
    src: crate::extensions::khr_ray_tracing::AccelerationStructureKHR,
    scratch: crate::vk1_0::Buffer,
    scratch_offset: crate::vk1_0::DeviceSize,
) -> std::ffi::c_void;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdTraceRaysNV.html) · Function"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdTraceRaysNV = unsafe extern "system" fn(
    command_buffer: crate::vk1_0::CommandBuffer,
    raygen_shader_binding_table_buffer: crate::vk1_0::Buffer,
    raygen_shader_binding_offset: crate::vk1_0::DeviceSize,
    miss_shader_binding_table_buffer: crate::vk1_0::Buffer,
    miss_shader_binding_offset: crate::vk1_0::DeviceSize,
    miss_shader_binding_stride: crate::vk1_0::DeviceSize,
    hit_shader_binding_table_buffer: crate::vk1_0::Buffer,
    hit_shader_binding_offset: crate::vk1_0::DeviceSize,
    hit_shader_binding_stride: crate::vk1_0::DeviceSize,
    callable_shader_binding_table_buffer: crate::vk1_0::Buffer,
    callable_shader_binding_offset: crate::vk1_0::DeviceSize,
    callable_shader_binding_stride: crate::vk1_0::DeviceSize,
    width: u32,
    height: u32,
    depth: u32,
) -> std::ffi::c_void;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetAccelerationStructureHandleNV.html) · Function"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetAccelerationStructureHandleNV = unsafe extern "system" fn(
    device: crate::vk1_0::Device,
    acceleration_structure: crate::extensions::khr_ray_tracing::AccelerationStructureKHR,
    data_size: usize,
    p_data: *mut std::ffi::c_void,
)
    -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateRayTracingPipelinesNV.html) · Function"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateRayTracingPipelinesNV = unsafe extern "system" fn(
    device: crate::vk1_0::Device,
    pipeline_cache: crate::vk1_0::PipelineCache,
    create_info_count: u32,
    p_create_infos: *const crate::extensions::nv_ray_tracing::RayTracingPipelineCreateInfoNV,
    p_allocator: *const crate::vk1_0::AllocationCallbacks,
    p_pipelines: *mut crate::vk1_0::Pipeline,
) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRayTracingShaderGroupCreateInfoNV.html) · Structure"]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct RayTracingShaderGroupCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub _type: crate::extensions::khr_ray_tracing::RayTracingShaderGroupTypeKHR,
    pub general_shader: u32,
    pub closest_hit_shader: u32,
    pub any_hit_shader: u32,
    pub intersection_shader: u32,
}
impl Default for RayTracingShaderGroupCreateInfoNV {
    fn default() -> Self {
        Self {
            s_type: crate::vk1_0::StructureType::RAY_TRACING_SHADER_GROUP_CREATE_INFO_NV,
            p_next: std::ptr::null(),
            _type: Default::default(),
            general_shader: Default::default(),
            closest_hit_shader: Default::default(),
            any_hit_shader: Default::default(),
            intersection_shader: Default::default(),
        }
    }
}
impl std::fmt::Debug for RayTracingShaderGroupCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("RayTracingShaderGroupCreateInfoNV")
            .field("s_type", &self.s_type)
            .field("p_next", &self.p_next)
            .field("_type", &self._type)
            .field("general_shader", &self.general_shader)
            .field("closest_hit_shader", &self.closest_hit_shader)
            .field("any_hit_shader", &self.any_hit_shader)
            .field("intersection_shader", &self.intersection_shader)
            .finish()
    }
}
impl RayTracingShaderGroupCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> RayTracingShaderGroupCreateInfoNVBuilder<'a> {
        RayTracingShaderGroupCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRayTracingShaderGroupCreateInfoNV.html) · Builder of [`RayTracingShaderGroupCreateInfoNV`](struct.RayTracingShaderGroupCreateInfoNV.html)"]
#[repr(transparent)]
pub struct RayTracingShaderGroupCreateInfoNVBuilder<'a>(
    RayTracingShaderGroupCreateInfoNV,
    std::marker::PhantomData<&'a ()>,
);
impl<'a> RayTracingShaderGroupCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> RayTracingShaderGroupCreateInfoNVBuilder<'a> {
        RayTracingShaderGroupCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn _type(
        mut self,
        _type: crate::extensions::khr_ray_tracing::RayTracingShaderGroupTypeKHR,
    ) -> Self {
        self.0._type = _type as _;
        self
    }
    #[inline]
    pub fn general_shader(mut self, general_shader: u32) -> Self {
        self.0.general_shader = general_shader as _;
        self
    }
    #[inline]
    pub fn closest_hit_shader(mut self, closest_hit_shader: u32) -> Self {
        self.0.closest_hit_shader = closest_hit_shader as _;
        self
    }
    #[inline]
    pub fn any_hit_shader(mut self, any_hit_shader: u32) -> Self {
        self.0.any_hit_shader = any_hit_shader as _;
        self
    }
    #[inline]
    pub fn intersection_shader(mut self, intersection_shader: u32) -> Self {
        self.0.intersection_shader = intersection_shader as _;
        self
    }
    #[inline]
    #[doc = "Discards all lifetime information. Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> RayTracingShaderGroupCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for RayTracingShaderGroupCreateInfoNVBuilder<'a> {
    fn default() -> RayTracingShaderGroupCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for RayTracingShaderGroupCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for RayTracingShaderGroupCreateInfoNVBuilder<'a> {
    type Target = RayTracingShaderGroupCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for RayTracingShaderGroupCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRayTracingPipelineCreateInfoNV.html) · Structure"]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct RayTracingPipelineCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::vk1_0::PipelineCreateFlags,
    pub stage_count: u32,
    pub p_stages: *const crate::vk1_0::PipelineShaderStageCreateInfo,
    pub group_count: u32,
    pub p_groups: *const crate::extensions::nv_ray_tracing::RayTracingShaderGroupCreateInfoNV,
    pub max_recursion_depth: u32,
    pub layout: crate::vk1_0::PipelineLayout,
    pub base_pipeline_handle: crate::vk1_0::Pipeline,
    pub base_pipeline_index: i32,
}
impl Default for RayTracingPipelineCreateInfoNV {
    fn default() -> Self {
        Self {
            s_type: crate::vk1_0::StructureType::RAY_TRACING_PIPELINE_CREATE_INFO_NV,
            p_next: std::ptr::null(),
            flags: Default::default(),
            stage_count: Default::default(),
            p_stages: std::ptr::null(),
            group_count: Default::default(),
            p_groups: std::ptr::null(),
            max_recursion_depth: Default::default(),
            layout: Default::default(),
            base_pipeline_handle: Default::default(),
            base_pipeline_index: Default::default(),
        }
    }
}
impl std::fmt::Debug for RayTracingPipelineCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("RayTracingPipelineCreateInfoNV")
            .field("s_type", &self.s_type)
            .field("p_next", &self.p_next)
            .field("flags", &self.flags)
            .field("stage_count", &self.stage_count)
            .field("p_stages", &self.p_stages)
            .field("group_count", &self.group_count)
            .field("p_groups", &self.p_groups)
            .field("max_recursion_depth", &self.max_recursion_depth)
            .field("layout", &self.layout)
            .field("base_pipeline_handle", &self.base_pipeline_handle)
            .field("base_pipeline_index", &self.base_pipeline_index)
            .finish()
    }
}
impl RayTracingPipelineCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> RayTracingPipelineCreateInfoNVBuilder<'a> {
        RayTracingPipelineCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
impl<'a>
    crate::ExtendableFrom<
        'a,
        crate::extensions::ext_pipeline_creation_feedback::PipelineCreationFeedbackCreateInfoEXT,
    > for RayTracingPipelineCreateInfoNVBuilder<'a>
{
}
impl < 'a > crate :: ExtendableFrom < 'a , crate :: extensions :: ext_pipeline_creation_feedback :: PipelineCreationFeedbackCreateInfoEXTBuilder < '_ >> for RayTracingPipelineCreateInfoNVBuilder < 'a > { }
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRayTracingPipelineCreateInfoNV.html) · Builder of [`RayTracingPipelineCreateInfoNV`](struct.RayTracingPipelineCreateInfoNV.html)"]
#[repr(transparent)]
pub struct RayTracingPipelineCreateInfoNVBuilder<'a>(
    RayTracingPipelineCreateInfoNV,
    std::marker::PhantomData<&'a ()>,
);
impl<'a> RayTracingPipelineCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> RayTracingPipelineCreateInfoNVBuilder<'a> {
        RayTracingPipelineCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::vk1_0::PipelineCreateFlags) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn stages(
        mut self,
        stages: &'a [crate::vk1_0::PipelineShaderStageCreateInfoBuilder],
    ) -> Self {
        self.0.p_stages = stages.as_ptr() as _;
        self.0.stage_count = stages.len() as _;
        self
    }
    #[inline]
    pub fn groups(
        mut self,
        groups: &'a [crate::extensions::nv_ray_tracing::RayTracingShaderGroupCreateInfoNVBuilder],
    ) -> Self {
        self.0.p_groups = groups.as_ptr() as _;
        self.0.group_count = groups.len() as _;
        self
    }
    #[inline]
    pub fn max_recursion_depth(mut self, max_recursion_depth: u32) -> Self {
        self.0.max_recursion_depth = max_recursion_depth as _;
        self
    }
    #[inline]
    pub fn layout(mut self, layout: crate::vk1_0::PipelineLayout) -> Self {
        self.0.layout = layout as _;
        self
    }
    #[inline]
    pub fn base_pipeline_handle(mut self, base_pipeline_handle: crate::vk1_0::Pipeline) -> Self {
        self.0.base_pipeline_handle = base_pipeline_handle as _;
        self
    }
    #[inline]
    pub fn base_pipeline_index(mut self, base_pipeline_index: i32) -> Self {
        self.0.base_pipeline_index = base_pipeline_index as _;
        self
    }
    #[inline]
    #[doc = "Discards all lifetime information. Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> RayTracingPipelineCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for RayTracingPipelineCreateInfoNVBuilder<'a> {
    fn default() -> RayTracingPipelineCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for RayTracingPipelineCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for RayTracingPipelineCreateInfoNVBuilder<'a> {
    type Target = RayTracingPipelineCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for RayTracingPipelineCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryTrianglesNV.html) · Structure"]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct GeometryTrianglesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub vertex_data: crate::vk1_0::Buffer,
    pub vertex_offset: crate::vk1_0::DeviceSize,
    pub vertex_count: u32,
    pub vertex_stride: crate::vk1_0::DeviceSize,
    pub vertex_format: crate::vk1_0::Format,
    pub index_data: crate::vk1_0::Buffer,
    pub index_offset: crate::vk1_0::DeviceSize,
    pub index_count: u32,
    pub index_type: crate::vk1_0::IndexType,
    pub transform_data: crate::vk1_0::Buffer,
    pub transform_offset: crate::vk1_0::DeviceSize,
}
impl Default for GeometryTrianglesNV {
    fn default() -> Self {
        Self {
            s_type: crate::vk1_0::StructureType::GEOMETRY_TRIANGLES_NV,
            p_next: std::ptr::null(),
            vertex_data: Default::default(),
            vertex_offset: Default::default(),
            vertex_count: Default::default(),
            vertex_stride: Default::default(),
            vertex_format: Default::default(),
            index_data: Default::default(),
            index_offset: Default::default(),
            index_count: Default::default(),
            index_type: Default::default(),
            transform_data: Default::default(),
            transform_offset: Default::default(),
        }
    }
}
impl std::fmt::Debug for GeometryTrianglesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("GeometryTrianglesNV")
            .field("s_type", &self.s_type)
            .field("p_next", &self.p_next)
            .field("vertex_data", &self.vertex_data)
            .field("vertex_offset", &self.vertex_offset)
            .field("vertex_count", &self.vertex_count)
            .field("vertex_stride", &self.vertex_stride)
            .field("vertex_format", &self.vertex_format)
            .field("index_data", &self.index_data)
            .field("index_offset", &self.index_offset)
            .field("index_count", &self.index_count)
            .field("index_type", &self.index_type)
            .field("transform_data", &self.transform_data)
            .field("transform_offset", &self.transform_offset)
            .finish()
    }
}
impl GeometryTrianglesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> GeometryTrianglesNVBuilder<'a> {
        GeometryTrianglesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryTrianglesNV.html) · Builder of [`GeometryTrianglesNV`](struct.GeometryTrianglesNV.html)"]
#[repr(transparent)]
pub struct GeometryTrianglesNVBuilder<'a>(GeometryTrianglesNV, std::marker::PhantomData<&'a ()>);
impl<'a> GeometryTrianglesNVBuilder<'a> {
    #[inline]
    pub fn new() -> GeometryTrianglesNVBuilder<'a> {
        GeometryTrianglesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn vertex_data(mut self, vertex_data: crate::vk1_0::Buffer) -> Self {
        self.0.vertex_data = vertex_data as _;
        self
    }
    #[inline]
    pub fn vertex_offset(mut self, vertex_offset: crate::vk1_0::DeviceSize) -> Self {
        self.0.vertex_offset = vertex_offset as _;
        self
    }
    #[inline]
    pub fn vertex_count(mut self, vertex_count: u32) -> Self {
        self.0.vertex_count = vertex_count as _;
        self
    }
    #[inline]
    pub fn vertex_stride(mut self, vertex_stride: crate::vk1_0::DeviceSize) -> Self {
        self.0.vertex_stride = vertex_stride as _;
        self
    }
    #[inline]
    pub fn vertex_format(mut self, vertex_format: crate::vk1_0::Format) -> Self {
        self.0.vertex_format = vertex_format as _;
        self
    }
    #[inline]
    pub fn index_data(mut self, index_data: crate::vk1_0::Buffer) -> Self {
        self.0.index_data = index_data as _;
        self
    }
    #[inline]
    pub fn index_offset(mut self, index_offset: crate::vk1_0::DeviceSize) -> Self {
        self.0.index_offset = index_offset as _;
        self
    }
    #[inline]
    pub fn index_count(mut self, index_count: u32) -> Self {
        self.0.index_count = index_count as _;
        self
    }
    #[inline]
    pub fn index_type(mut self, index_type: crate::vk1_0::IndexType) -> Self {
        self.0.index_type = index_type as _;
        self
    }
    #[inline]
    pub fn transform_data(mut self, transform_data: crate::vk1_0::Buffer) -> Self {
        self.0.transform_data = transform_data as _;
        self
    }
    #[inline]
    pub fn transform_offset(mut self, transform_offset: crate::vk1_0::DeviceSize) -> Self {
        self.0.transform_offset = transform_offset as _;
        self
    }
    #[inline]
    #[doc = "Discards all lifetime information. Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> GeometryTrianglesNV {
        self.0
    }
}
impl<'a> std::default::Default for GeometryTrianglesNVBuilder<'a> {
    fn default() -> GeometryTrianglesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for GeometryTrianglesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for GeometryTrianglesNVBuilder<'a> {
    type Target = GeometryTrianglesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for GeometryTrianglesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryAABBNV.html) · Structure"]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct GeometryAABBNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub aabb_data: crate::vk1_0::Buffer,
    pub num_aab_bs: u32,
    pub stride: u32,
    pub offset: crate::vk1_0::DeviceSize,
}
impl Default for GeometryAABBNV {
    fn default() -> Self {
        Self {
            s_type: crate::vk1_0::StructureType::GEOMETRY_AABB_NV,
            p_next: std::ptr::null(),
            aabb_data: Default::default(),
            num_aab_bs: Default::default(),
            stride: Default::default(),
            offset: Default::default(),
        }
    }
}
impl std::fmt::Debug for GeometryAABBNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("GeometryAABBNV")
            .field("s_type", &self.s_type)
            .field("p_next", &self.p_next)
            .field("aabb_data", &self.aabb_data)
            .field("num_aab_bs", &self.num_aab_bs)
            .field("stride", &self.stride)
            .field("offset", &self.offset)
            .finish()
    }
}
impl GeometryAABBNV {
    #[inline]
    pub fn into_builder<'a>(self) -> GeometryAABBNVBuilder<'a> {
        GeometryAABBNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryAABBNV.html) · Builder of [`GeometryAABBNV`](struct.GeometryAABBNV.html)"]
#[repr(transparent)]
pub struct GeometryAABBNVBuilder<'a>(GeometryAABBNV, std::marker::PhantomData<&'a ()>);
impl<'a> GeometryAABBNVBuilder<'a> {
    #[inline]
    pub fn new() -> GeometryAABBNVBuilder<'a> {
        GeometryAABBNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn aabb_data(mut self, aabb_data: crate::vk1_0::Buffer) -> Self {
        self.0.aabb_data = aabb_data as _;
        self
    }
    #[inline]
    pub fn num_aab_bs(mut self, num_aab_bs: u32) -> Self {
        self.0.num_aab_bs = num_aab_bs as _;
        self
    }
    #[inline]
    pub fn stride(mut self, stride: u32) -> Self {
        self.0.stride = stride as _;
        self
    }
    #[inline]
    pub fn offset(mut self, offset: crate::vk1_0::DeviceSize) -> Self {
        self.0.offset = offset as _;
        self
    }
    #[inline]
    #[doc = "Discards all lifetime information. Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> GeometryAABBNV {
        self.0
    }
}
impl<'a> std::default::Default for GeometryAABBNVBuilder<'a> {
    fn default() -> GeometryAABBNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for GeometryAABBNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for GeometryAABBNVBuilder<'a> {
    type Target = GeometryAABBNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for GeometryAABBNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryDataNV.html) · Structure"]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct GeometryDataNV {
    pub triangles: crate::extensions::nv_ray_tracing::GeometryTrianglesNV,
    pub aabbs: crate::extensions::nv_ray_tracing::GeometryAABBNV,
}
impl Default for GeometryDataNV {
    fn default() -> Self {
        Self {
            triangles: Default::default(),
            aabbs: Default::default(),
        }
    }
}
impl std::fmt::Debug for GeometryDataNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("GeometryDataNV")
            .field("triangles", &self.triangles)
            .field("aabbs", &self.aabbs)
            .finish()
    }
}
impl GeometryDataNV {
    #[inline]
    pub fn into_builder<'a>(self) -> GeometryDataNVBuilder<'a> {
        GeometryDataNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryDataNV.html) · Builder of [`GeometryDataNV`](struct.GeometryDataNV.html)"]
#[repr(transparent)]
pub struct GeometryDataNVBuilder<'a>(GeometryDataNV, std::marker::PhantomData<&'a ()>);
impl<'a> GeometryDataNVBuilder<'a> {
    #[inline]
    pub fn new() -> GeometryDataNVBuilder<'a> {
        GeometryDataNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn triangles(
        mut self,
        triangles: crate::extensions::nv_ray_tracing::GeometryTrianglesNV,
    ) -> Self {
        self.0.triangles = triangles as _;
        self
    }
    #[inline]
    pub fn aabbs(mut self, aabbs: crate::extensions::nv_ray_tracing::GeometryAABBNV) -> Self {
        self.0.aabbs = aabbs as _;
        self
    }
    #[inline]
    #[doc = "Discards all lifetime information. Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> GeometryDataNV {
        self.0
    }
}
impl<'a> std::default::Default for GeometryDataNVBuilder<'a> {
    fn default() -> GeometryDataNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for GeometryDataNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for GeometryDataNVBuilder<'a> {
    type Target = GeometryDataNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for GeometryDataNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryNV.html) · Structure"]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct GeometryNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub geometry_type: crate::extensions::khr_ray_tracing::GeometryTypeKHR,
    pub geometry: crate::extensions::nv_ray_tracing::GeometryDataNV,
    pub flags: crate::extensions::khr_ray_tracing::GeometryFlagsKHR,
}
impl Default for GeometryNV {
    fn default() -> Self {
        Self {
            s_type: crate::vk1_0::StructureType::GEOMETRY_NV,
            p_next: std::ptr::null(),
            geometry_type: Default::default(),
            geometry: Default::default(),
            flags: Default::default(),
        }
    }
}
impl std::fmt::Debug for GeometryNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("GeometryNV")
            .field("s_type", &self.s_type)
            .field("p_next", &self.p_next)
            .field("geometry_type", &self.geometry_type)
            .field("geometry", &self.geometry)
            .field("flags", &self.flags)
            .finish()
    }
}
impl GeometryNV {
    #[inline]
    pub fn into_builder<'a>(self) -> GeometryNVBuilder<'a> {
        GeometryNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryNV.html) · Builder of [`GeometryNV`](struct.GeometryNV.html)"]
#[repr(transparent)]
pub struct GeometryNVBuilder<'a>(GeometryNV, std::marker::PhantomData<&'a ()>);
impl<'a> GeometryNVBuilder<'a> {
    #[inline]
    pub fn new() -> GeometryNVBuilder<'a> {
        GeometryNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn geometry_type(
        mut self,
        geometry_type: crate::extensions::khr_ray_tracing::GeometryTypeKHR,
    ) -> Self {
        self.0.geometry_type = geometry_type as _;
        self
    }
    #[inline]
    pub fn geometry(mut self, geometry: crate::extensions::nv_ray_tracing::GeometryDataNV) -> Self {
        self.0.geometry = geometry as _;
        self
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_ray_tracing::GeometryFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    #[doc = "Discards all lifetime information. Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> GeometryNV {
        self.0
    }
}
impl<'a> std::default::Default for GeometryNVBuilder<'a> {
    fn default() -> GeometryNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for GeometryNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for GeometryNVBuilder<'a> {
    type Target = GeometryNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for GeometryNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureInfoNV.html) · Structure"]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AccelerationStructureInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub _type: crate::extensions::nv_ray_tracing::AccelerationStructureTypeNV,
    pub flags: crate::extensions::nv_ray_tracing::BuildAccelerationStructureFlagsNV,
    pub instance_count: u32,
    pub geometry_count: u32,
    pub p_geometries: *const crate::extensions::nv_ray_tracing::GeometryNV,
}
impl Default for AccelerationStructureInfoNV {
    fn default() -> Self {
        Self {
            s_type: crate::vk1_0::StructureType::ACCELERATION_STRUCTURE_INFO_NV,
            p_next: std::ptr::null(),
            _type: Default::default(),
            flags: Default::default(),
            instance_count: Default::default(),
            geometry_count: Default::default(),
            p_geometries: std::ptr::null(),
        }
    }
}
impl std::fmt::Debug for AccelerationStructureInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AccelerationStructureInfoNV")
            .field("s_type", &self.s_type)
            .field("p_next", &self.p_next)
            .field("_type", &self._type)
            .field("flags", &self.flags)
            .field("instance_count", &self.instance_count)
            .field("geometry_count", &self.geometry_count)
            .field("p_geometries", &self.p_geometries)
            .finish()
    }
}
impl AccelerationStructureInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> AccelerationStructureInfoNVBuilder<'a> {
        AccelerationStructureInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureInfoNV.html) · Builder of [`AccelerationStructureInfoNV`](struct.AccelerationStructureInfoNV.html)"]
#[repr(transparent)]
pub struct AccelerationStructureInfoNVBuilder<'a>(
    AccelerationStructureInfoNV,
    std::marker::PhantomData<&'a ()>,
);
impl<'a> AccelerationStructureInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> AccelerationStructureInfoNVBuilder<'a> {
        AccelerationStructureInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn _type(
        mut self,
        _type: crate::extensions::nv_ray_tracing::AccelerationStructureTypeNV,
    ) -> Self {
        self.0._type = _type as _;
        self
    }
    #[inline]
    pub fn flags(
        mut self,
        flags: crate::extensions::nv_ray_tracing::BuildAccelerationStructureFlagsNV,
    ) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn instance_count(mut self, instance_count: u32) -> Self {
        self.0.instance_count = instance_count as _;
        self
    }
    #[inline]
    pub fn geometries(
        mut self,
        geometries: &'a [crate::extensions::nv_ray_tracing::GeometryNVBuilder],
    ) -> Self {
        self.0.p_geometries = geometries.as_ptr() as _;
        self.0.geometry_count = geometries.len() as _;
        self
    }
    #[inline]
    #[doc = "Discards all lifetime information. Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AccelerationStructureInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for AccelerationStructureInfoNVBuilder<'a> {
    fn default() -> AccelerationStructureInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AccelerationStructureInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AccelerationStructureInfoNVBuilder<'a> {
    type Target = AccelerationStructureInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AccelerationStructureInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureCreateInfoNV.html) · Structure"]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AccelerationStructureCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub compacted_size: crate::vk1_0::DeviceSize,
    pub info: crate::extensions::nv_ray_tracing::AccelerationStructureInfoNV,
}
impl Default for AccelerationStructureCreateInfoNV {
    fn default() -> Self {
        Self {
            s_type: crate::vk1_0::StructureType::ACCELERATION_STRUCTURE_CREATE_INFO_NV,
            p_next: std::ptr::null(),
            compacted_size: Default::default(),
            info: Default::default(),
        }
    }
}
impl std::fmt::Debug for AccelerationStructureCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AccelerationStructureCreateInfoNV")
            .field("s_type", &self.s_type)
            .field("p_next", &self.p_next)
            .field("compacted_size", &self.compacted_size)
            .field("info", &self.info)
            .finish()
    }
}
impl AccelerationStructureCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> AccelerationStructureCreateInfoNVBuilder<'a> {
        AccelerationStructureCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureCreateInfoNV.html) · Builder of [`AccelerationStructureCreateInfoNV`](struct.AccelerationStructureCreateInfoNV.html)"]
#[repr(transparent)]
pub struct AccelerationStructureCreateInfoNVBuilder<'a>(
    AccelerationStructureCreateInfoNV,
    std::marker::PhantomData<&'a ()>,
);
impl<'a> AccelerationStructureCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> AccelerationStructureCreateInfoNVBuilder<'a> {
        AccelerationStructureCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn compacted_size(mut self, compacted_size: crate::vk1_0::DeviceSize) -> Self {
        self.0.compacted_size = compacted_size as _;
        self
    }
    #[inline]
    pub fn info(
        mut self,
        info: crate::extensions::nv_ray_tracing::AccelerationStructureInfoNV,
    ) -> Self {
        self.0.info = info as _;
        self
    }
    #[inline]
    #[doc = "Discards all lifetime information. Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AccelerationStructureCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for AccelerationStructureCreateInfoNVBuilder<'a> {
    fn default() -> AccelerationStructureCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AccelerationStructureCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AccelerationStructureCreateInfoNVBuilder<'a> {
    type Target = AccelerationStructureCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AccelerationStructureCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMemoryRequirementsInfoNV.html) · Structure"]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AccelerationStructureMemoryRequirementsInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub _type: crate::extensions::nv_ray_tracing::AccelerationStructureMemoryRequirementsTypeNV,
    pub acceleration_structure: crate::extensions::nv_ray_tracing::AccelerationStructureNV,
}
impl Default for AccelerationStructureMemoryRequirementsInfoNV {
    fn default() -> Self {
        Self {
            s_type: crate::vk1_0::StructureType::ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV,
            p_next: std::ptr::null(),
            _type: Default::default(),
            acceleration_structure: Default::default(),
        }
    }
}
impl std::fmt::Debug for AccelerationStructureMemoryRequirementsInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AccelerationStructureMemoryRequirementsInfoNV")
            .field("s_type", &self.s_type)
            .field("p_next", &self.p_next)
            .field("_type", &self._type)
            .field("acceleration_structure", &self.acceleration_structure)
            .finish()
    }
}
impl AccelerationStructureMemoryRequirementsInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> AccelerationStructureMemoryRequirementsInfoNVBuilder<'a> {
        AccelerationStructureMemoryRequirementsInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMemoryRequirementsInfoNV.html) · Builder of [`AccelerationStructureMemoryRequirementsInfoNV`](struct.AccelerationStructureMemoryRequirementsInfoNV.html)"]
#[repr(transparent)]
pub struct AccelerationStructureMemoryRequirementsInfoNVBuilder<'a>(
    AccelerationStructureMemoryRequirementsInfoNV,
    std::marker::PhantomData<&'a ()>,
);
impl<'a> AccelerationStructureMemoryRequirementsInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> AccelerationStructureMemoryRequirementsInfoNVBuilder<'a> {
        AccelerationStructureMemoryRequirementsInfoNVBuilder(
            Default::default(),
            std::marker::PhantomData,
        )
    }
    #[inline]
    pub fn _type(
        mut self,
        _type: crate::extensions::nv_ray_tracing::AccelerationStructureMemoryRequirementsTypeNV,
    ) -> Self {
        self.0._type = _type as _;
        self
    }
    #[inline]
    pub fn acceleration_structure(
        mut self,
        acceleration_structure: crate::extensions::nv_ray_tracing::AccelerationStructureNV,
    ) -> Self {
        self.0.acceleration_structure = acceleration_structure as _;
        self
    }
    #[inline]
    #[doc = "Discards all lifetime information. Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AccelerationStructureMemoryRequirementsInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for AccelerationStructureMemoryRequirementsInfoNVBuilder<'a> {
    fn default() -> AccelerationStructureMemoryRequirementsInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AccelerationStructureMemoryRequirementsInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AccelerationStructureMemoryRequirementsInfoNVBuilder<'a> {
    type Target = AccelerationStructureMemoryRequirementsInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AccelerationStructureMemoryRequirementsInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRayTracingPropertiesNV.html) · Structure"]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceRayTracingPropertiesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub shader_group_handle_size: u32,
    pub max_recursion_depth: u32,
    pub max_shader_group_stride: u32,
    pub shader_group_base_alignment: u32,
    pub max_geometry_count: u64,
    pub max_instance_count: u64,
    pub max_triangle_count: u64,
    pub max_descriptor_set_acceleration_structures: u32,
}
impl Default for PhysicalDeviceRayTracingPropertiesNV {
    fn default() -> Self {
        Self {
            s_type: crate::vk1_0::StructureType::PHYSICAL_DEVICE_RAY_TRACING_PROPERTIES_NV,
            p_next: std::ptr::null_mut(),
            shader_group_handle_size: Default::default(),
            max_recursion_depth: Default::default(),
            max_shader_group_stride: Default::default(),
            shader_group_base_alignment: Default::default(),
            max_geometry_count: Default::default(),
            max_instance_count: Default::default(),
            max_triangle_count: Default::default(),
            max_descriptor_set_acceleration_structures: Default::default(),
        }
    }
}
impl std::fmt::Debug for PhysicalDeviceRayTracingPropertiesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceRayTracingPropertiesNV")
            .field("s_type", &self.s_type)
            .field("p_next", &self.p_next)
            .field("shader_group_handle_size", &self.shader_group_handle_size)
            .field("max_recursion_depth", &self.max_recursion_depth)
            .field("max_shader_group_stride", &self.max_shader_group_stride)
            .field(
                "shader_group_base_alignment",
                &self.shader_group_base_alignment,
            )
            .field("max_geometry_count", &self.max_geometry_count)
            .field("max_instance_count", &self.max_instance_count)
            .field("max_triangle_count", &self.max_triangle_count)
            .field(
                "max_descriptor_set_acceleration_structures",
                &self.max_descriptor_set_acceleration_structures,
            )
            .finish()
    }
}
impl PhysicalDeviceRayTracingPropertiesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceRayTracingPropertiesNVBuilder<'a> {
        PhysicalDeviceRayTracingPropertiesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRayTracingPropertiesNV.html) · Builder of [`PhysicalDeviceRayTracingPropertiesNV`](struct.PhysicalDeviceRayTracingPropertiesNV.html)"]
#[repr(transparent)]
pub struct PhysicalDeviceRayTracingPropertiesNVBuilder<'a>(
    PhysicalDeviceRayTracingPropertiesNV,
    std::marker::PhantomData<&'a ()>,
);
impl<'a> PhysicalDeviceRayTracingPropertiesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceRayTracingPropertiesNVBuilder<'a> {
        PhysicalDeviceRayTracingPropertiesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shader_group_handle_size(mut self, shader_group_handle_size: u32) -> Self {
        self.0.shader_group_handle_size = shader_group_handle_size as _;
        self
    }
    #[inline]
    pub fn max_recursion_depth(mut self, max_recursion_depth: u32) -> Self {
        self.0.max_recursion_depth = max_recursion_depth as _;
        self
    }
    #[inline]
    pub fn max_shader_group_stride(mut self, max_shader_group_stride: u32) -> Self {
        self.0.max_shader_group_stride = max_shader_group_stride as _;
        self
    }
    #[inline]
    pub fn shader_group_base_alignment(mut self, shader_group_base_alignment: u32) -> Self {
        self.0.shader_group_base_alignment = shader_group_base_alignment as _;
        self
    }
    #[inline]
    pub fn max_geometry_count(mut self, max_geometry_count: u64) -> Self {
        self.0.max_geometry_count = max_geometry_count as _;
        self
    }
    #[inline]
    pub fn max_instance_count(mut self, max_instance_count: u64) -> Self {
        self.0.max_instance_count = max_instance_count as _;
        self
    }
    #[inline]
    pub fn max_triangle_count(mut self, max_triangle_count: u64) -> Self {
        self.0.max_triangle_count = max_triangle_count as _;
        self
    }
    #[inline]
    pub fn max_descriptor_set_acceleration_structures(
        mut self,
        max_descriptor_set_acceleration_structures: u32,
    ) -> Self {
        self.0.max_descriptor_set_acceleration_structures =
            max_descriptor_set_acceleration_structures as _;
        self
    }
    #[inline]
    #[doc = "Discards all lifetime information. Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceRayTracingPropertiesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceRayTracingPropertiesNVBuilder<'a> {
    fn default() -> PhysicalDeviceRayTracingPropertiesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceRayTracingPropertiesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceRayTracingPropertiesNVBuilder<'a> {
    type Target = PhysicalDeviceRayTracingPropertiesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceRayTracingPropertiesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`extensions::nv_ray_tracing`](extensions/nv_ray_tracing/index.html)"]
impl crate::DeviceLoader {
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCompileDeferredNV.html) · Function"]
    pub unsafe fn compile_deferred_nv(
        &self,
        pipeline: crate::vk1_0::Pipeline,
        shader: u32,
    ) -> crate::utils::VulkanResult<()> {
        let _function = self
            .compile_deferred_nv
            .expect("`compile_deferred_nv` is not loaded");
        let _return = _function(self.handle, pipeline as _, shader as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateAccelerationStructureNV.html) · Function"]
    pub unsafe fn create_acceleration_structure_nv(
        &self,
        create_info: &crate::extensions::nv_ray_tracing::AccelerationStructureCreateInfoNV,
        allocator: Option<&crate::vk1_0::AllocationCallbacks>,
        acceleration_structure: Option<crate::extensions::nv_ray_tracing::AccelerationStructureNV>,
    ) -> crate::utils::VulkanResult<crate::extensions::nv_ray_tracing::AccelerationStructureNV>
    {
        let _function = self
            .create_acceleration_structure_nv
            .expect("`create_acceleration_structure_nv` is not loaded");
        let mut acceleration_structure = match acceleration_structure {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut acceleration_structure,
        );
        crate::utils::VulkanResult::new(_return, acceleration_structure)
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetAccelerationStructureMemoryRequirementsNV.html) · Function"]
    pub unsafe fn get_acceleration_structure_memory_requirements_nv(
        &self,
        info: &crate::extensions::nv_ray_tracing::AccelerationStructureMemoryRequirementsInfoNV,
        memory_requirements: Option<
            crate::extensions::khr_get_memory_requirements2::MemoryRequirements2KHR,
        >,
    ) -> crate::extensions::khr_get_memory_requirements2::MemoryRequirements2KHR {
        let _function = self
            .get_acceleration_structure_memory_requirements_nv
            .expect("`get_acceleration_structure_memory_requirements_nv` is not loaded");
        let mut memory_requirements = match memory_requirements {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(self.handle, info as _, &mut memory_requirements);
        memory_requirements
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdCopyAccelerationStructureNV.html) · Function"]
    pub unsafe fn cmd_copy_acceleration_structure_nv(
        &self,
        command_buffer: crate::vk1_0::CommandBuffer,
        dst: crate::extensions::khr_ray_tracing::AccelerationStructureKHR,
        src: crate::extensions::khr_ray_tracing::AccelerationStructureKHR,
        mode: crate::extensions::khr_ray_tracing::CopyAccelerationStructureModeKHR,
    ) -> () {
        let _function = self
            .cmd_copy_acceleration_structure_nv
            .expect("`cmd_copy_acceleration_structure_nv` is not loaded");
        let _return = _function(command_buffer as _, dst as _, src as _, mode as _);
        ()
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBuildAccelerationStructureNV.html) · Function"]
    pub unsafe fn cmd_build_acceleration_structure_nv(
        &self,
        command_buffer: crate::vk1_0::CommandBuffer,
        info: &crate::extensions::nv_ray_tracing::AccelerationStructureInfoNV,
        instance_data: Option<crate::vk1_0::Buffer>,
        instance_offset: crate::vk1_0::DeviceSize,
        update: bool,
        dst: crate::extensions::khr_ray_tracing::AccelerationStructureKHR,
        src: Option<crate::extensions::khr_ray_tracing::AccelerationStructureKHR>,
        scratch: crate::vk1_0::Buffer,
        scratch_offset: crate::vk1_0::DeviceSize,
    ) -> () {
        let _function = self
            .cmd_build_acceleration_structure_nv
            .expect("`cmd_build_acceleration_structure_nv` is not loaded");
        let _return = _function(
            command_buffer as _,
            info as _,
            match instance_data {
                Some(v) => v,
                None => Default::default(),
            },
            instance_offset as _,
            update as _,
            dst as _,
            match src {
                Some(v) => v,
                None => Default::default(),
            },
            scratch as _,
            scratch_offset as _,
        );
        ()
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdTraceRaysNV.html) · Function"]
    pub unsafe fn cmd_trace_rays_nv(
        &self,
        command_buffer: crate::vk1_0::CommandBuffer,
        raygen_shader_binding_table_buffer: crate::vk1_0::Buffer,
        raygen_shader_binding_offset: crate::vk1_0::DeviceSize,
        miss_shader_binding_table_buffer: Option<crate::vk1_0::Buffer>,
        miss_shader_binding_offset: crate::vk1_0::DeviceSize,
        miss_shader_binding_stride: crate::vk1_0::DeviceSize,
        hit_shader_binding_table_buffer: Option<crate::vk1_0::Buffer>,
        hit_shader_binding_offset: crate::vk1_0::DeviceSize,
        hit_shader_binding_stride: crate::vk1_0::DeviceSize,
        callable_shader_binding_table_buffer: Option<crate::vk1_0::Buffer>,
        callable_shader_binding_offset: crate::vk1_0::DeviceSize,
        callable_shader_binding_stride: crate::vk1_0::DeviceSize,
        width: u32,
        height: u32,
        depth: u32,
    ) -> () {
        let _function = self
            .cmd_trace_rays_nv
            .expect("`cmd_trace_rays_nv` is not loaded");
        let _return = _function(
            command_buffer as _,
            raygen_shader_binding_table_buffer as _,
            raygen_shader_binding_offset as _,
            match miss_shader_binding_table_buffer {
                Some(v) => v,
                None => Default::default(),
            },
            miss_shader_binding_offset as _,
            miss_shader_binding_stride as _,
            match hit_shader_binding_table_buffer {
                Some(v) => v,
                None => Default::default(),
            },
            hit_shader_binding_offset as _,
            hit_shader_binding_stride as _,
            match callable_shader_binding_table_buffer {
                Some(v) => v,
                None => Default::default(),
            },
            callable_shader_binding_offset as _,
            callable_shader_binding_stride as _,
            width as _,
            height as _,
            depth as _,
        );
        ()
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetAccelerationStructureHandleNV.html) · Function"]
    pub unsafe fn get_acceleration_structure_handle_nv(
        &self,
        acceleration_structure: crate::extensions::khr_ray_tracing::AccelerationStructureKHR,
        data_size: usize,
        data: *mut std::ffi::c_void,
    ) -> crate::utils::VulkanResult<()> {
        let _function = self
            .get_acceleration_structure_handle_nv
            .expect("`get_acceleration_structure_handle_nv` is not loaded");
        let _return = _function(self.handle, acceleration_structure as _, data_size, data);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateRayTracingPipelinesNV.html) · Function"]
    pub unsafe fn create_ray_tracing_pipelines_nv(
        &self,
        pipeline_cache: Option<crate::vk1_0::PipelineCache>,
        create_infos: &[crate::extensions::nv_ray_tracing::RayTracingPipelineCreateInfoNVBuilder],
        allocator: Option<&crate::vk1_0::AllocationCallbacks>,
    ) -> crate::utils::VulkanResult<Vec<crate::vk1_0::Pipeline>> {
        let _function = self
            .create_ray_tracing_pipelines_nv
            .expect("`create_ray_tracing_pipelines_nv` is not loaded");
        let create_info_count = create_infos.len();
        let mut pipelines = vec![Default::default(); create_info_count as _];
        let _return = _function(
            self.handle,
            match pipeline_cache {
                Some(v) => v,
                None => Default::default(),
            },
            create_info_count as _,
            create_infos.as_ptr() as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            pipelines.as_mut_ptr(),
        );
        crate::utils::VulkanResult::new(_return, pipelines)
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyAccelerationStructureNV.html) · Function"]
    pub unsafe fn destroy_acceleration_structure_nv(
        &self,
        acceleration_structure: Option<
            crate::extensions::khr_ray_tracing::AccelerationStructureKHR,
        >,
        allocator: Option<&crate::vk1_0::AllocationCallbacks>,
    ) -> () {
        let _function = self
            .destroy_acceleration_structure_nv
            .expect("`destroy_acceleration_structure_nv` is not loaded");
        let _return = _function(
            self.handle,
            match acceleration_structure {
                Some(v) => v,
                None => Default::default(),
            },
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
        );
        ()
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkBindAccelerationStructureMemoryNV.html) · Function"]
    pub unsafe fn bind_acceleration_structure_memory_nv(
        &self,
        bind_infos : & [ crate :: extensions :: khr_ray_tracing :: BindAccelerationStructureMemoryInfoKHRBuilder ],
    ) -> crate::utils::VulkanResult<()> {
        let _function = self
            .bind_acceleration_structure_memory_nv
            .expect("`bind_acceleration_structure_memory_nv` is not loaded");
        let bind_info_count = bind_infos.len();
        let _return = _function(self.handle, bind_info_count as _, bind_infos.as_ptr() as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdWriteAccelerationStructuresPropertiesNV.html) · Function"]
    pub unsafe fn cmd_write_acceleration_structures_properties_nv(
        &self,
        command_buffer: crate::vk1_0::CommandBuffer,
        acceleration_structures: &[crate::extensions::khr_ray_tracing::AccelerationStructureKHR],
        query_type: crate::vk1_0::QueryType,
        query_pool: crate::vk1_0::QueryPool,
        first_query: u32,
    ) -> () {
        let _function = self
            .cmd_write_acceleration_structures_properties_nv
            .expect("`cmd_write_acceleration_structures_properties_nv` is not loaded");
        let acceleration_structure_count = acceleration_structures.len();
        let _return = _function(
            command_buffer as _,
            acceleration_structure_count as _,
            acceleration_structures.as_ptr() as _,
            query_type as _,
            query_pool as _,
            first_query as _,
        );
        ()
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetRayTracingShaderGroupHandlesNV.html) · Function"]
    pub unsafe fn get_ray_tracing_shader_group_handles_nv(
        &self,
        pipeline: crate::vk1_0::Pipeline,
        first_group: u32,
        group_count: u32,
        data_size: usize,
        data: *mut std::ffi::c_void,
    ) -> crate::utils::VulkanResult<()> {
        let _function = self
            .get_ray_tracing_shader_group_handles_nv
            .expect("`get_ray_tracing_shader_group_handles_nv` is not loaded");
        let _return = _function(
            self.handle,
            pipeline as _,
            first_group as _,
            group_count as _,
            data_size,
            data,
        );
        crate::utils::VulkanResult::new(_return, ())
    }
}
