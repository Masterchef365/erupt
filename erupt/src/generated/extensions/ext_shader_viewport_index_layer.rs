#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_SHADER_VIEWPORT_INDEX_LAYER_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_SHADER_VIEWPORT_INDEX_LAYER_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_EXT_shader_viewport_index_layer");
