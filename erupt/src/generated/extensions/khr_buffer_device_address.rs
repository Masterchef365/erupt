#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_BUFFER_DEVICE_ADDRESS_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_BUFFER_DEVICE_ADDRESS_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_buffer_device_address");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_BUFFER_OPAQUE_CAPTURE_ADDRESS_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkGetBufferOpaqueCaptureAddressKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_BUFFER_DEVICE_ADDRESS_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkGetBufferDeviceAddressKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_DEVICE_MEMORY_OPAQUE_CAPTURE_ADDRESS_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkGetDeviceMemoryOpaqueCaptureAddressKHR");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceBufferDeviceAddressFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceBufferDeviceAddressFeaturesKHR =
    crate::vk1_2::PhysicalDeviceBufferDeviceAddressFeatures;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceBufferDeviceAddressFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceBufferDeviceAddressFeaturesKHRBuilder<'a> =
    crate::vk1_2::PhysicalDeviceBufferDeviceAddressFeaturesBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBufferDeviceAddressInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BufferDeviceAddressInfoKHR = crate::vk1_2::BufferDeviceAddressInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBufferDeviceAddressInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BufferDeviceAddressInfoKHRBuilder<'a> = crate::vk1_2::BufferDeviceAddressInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBufferOpaqueCaptureAddressCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BufferOpaqueCaptureAddressCreateInfoKHR =
    crate::vk1_2::BufferOpaqueCaptureAddressCreateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBufferOpaqueCaptureAddressCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BufferOpaqueCaptureAddressCreateInfoKHRBuilder<'a> =
    crate::vk1_2::BufferOpaqueCaptureAddressCreateInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryOpaqueCaptureAddressAllocateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type MemoryOpaqueCaptureAddressAllocateInfoKHR =
    crate::vk1_2::MemoryOpaqueCaptureAddressAllocateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryOpaqueCaptureAddressAllocateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type MemoryOpaqueCaptureAddressAllocateInfoKHRBuilder<'a> =
    crate::vk1_2::MemoryOpaqueCaptureAddressAllocateInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceMemoryOpaqueCaptureAddressInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DeviceMemoryOpaqueCaptureAddressInfoKHR =
    crate::vk1_2::DeviceMemoryOpaqueCaptureAddressInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceMemoryOpaqueCaptureAddressInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DeviceMemoryOpaqueCaptureAddressInfoKHRBuilder<'a> =
    crate::vk1_2::DeviceMemoryOpaqueCaptureAddressInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetBufferOpaqueCaptureAddressKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetBufferOpaqueCaptureAddressKHR = crate::vk1_2::PFN_vkGetBufferOpaqueCaptureAddress;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetBufferDeviceAddressKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetBufferDeviceAddressKHR = crate::vk1_2::PFN_vkGetBufferDeviceAddress;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDeviceMemoryOpaqueCaptureAddressKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetDeviceMemoryOpaqueCaptureAddressKHR =
    crate::vk1_2::PFN_vkGetDeviceMemoryOpaqueCaptureAddress;
#[doc = "Provided by [`extensions::khr_buffer_device_address`](extensions/khr_buffer_device_address/index.html)"]
impl crate::DeviceLoader {
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetBufferOpaqueCaptureAddressKHR.html) · Function"]
    pub unsafe fn get_buffer_opaque_capture_address_khr(
        &self,
        info: &crate::vk1_2::BufferDeviceAddressInfo,
    ) -> u64 {
        let _function = self
            .get_buffer_opaque_capture_address_khr
            .expect("`get_buffer_opaque_capture_address_khr` is not loaded");
        let _return = _function(self.handle, info as _);
        _return
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetBufferDeviceAddressKHR.html) · Function"]
    pub unsafe fn get_buffer_device_address_khr(
        &self,
        info: &crate::vk1_2::BufferDeviceAddressInfo,
    ) -> crate::vk1_0::DeviceAddress {
        let _function = self
            .get_buffer_device_address_khr
            .expect("`get_buffer_device_address_khr` is not loaded");
        let _return = _function(self.handle, info as _);
        _return
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDeviceMemoryOpaqueCaptureAddressKHR.html) · Function"]
    pub unsafe fn get_device_memory_opaque_capture_address_khr(
        &self,
        info: &crate::vk1_2::DeviceMemoryOpaqueCaptureAddressInfo,
    ) -> u64 {
        let _function = self
            .get_device_memory_opaque_capture_address_khr
            .expect("`get_device_memory_opaque_capture_address_khr` is not loaded");
        let _return = _function(self.handle, info as _);
        _return
    }
}
