#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_IMAGE_FORMAT_LIST_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_IMAGE_FORMAT_LIST_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_image_format_list");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageFormatListCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ImageFormatListCreateInfoKHR = crate::vk1_2::ImageFormatListCreateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageFormatListCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ImageFormatListCreateInfoKHRBuilder<'a> =
    crate::vk1_2::ImageFormatListCreateInfoBuilder<'a>;
