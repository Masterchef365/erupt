#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const NV_SHADER_SUBGROUP_PARTITIONED_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const NV_SHADER_SUBGROUP_PARTITIONED_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_NV_shader_subgroup_partitioned");
