#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_DEPTH_STENCIL_RESOLVE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_DEPTH_STENCIL_RESOLVE_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_depth_stencil_resolve");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkResolveModeFlagsKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ResolveModeFlagsKHR = crate::vk1_2::ResolveModeFlags;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkResolveModeFlagBitsKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ResolveModeFlagBitsKHR = crate::vk1_2::ResolveModeFlagBits;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDepthStencilResolvePropertiesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceDepthStencilResolvePropertiesKHR =
    crate::vk1_2::PhysicalDeviceDepthStencilResolveProperties;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDepthStencilResolvePropertiesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceDepthStencilResolvePropertiesKHRBuilder<'a> =
    crate::vk1_2::PhysicalDeviceDepthStencilResolvePropertiesBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassDescriptionDepthStencilResolveKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SubpassDescriptionDepthStencilResolveKHR =
    crate::vk1_2::SubpassDescriptionDepthStencilResolve;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassDescriptionDepthStencilResolveKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SubpassDescriptionDepthStencilResolveKHRBuilder<'a> =
    crate::vk1_2::SubpassDescriptionDepthStencilResolveBuilder<'a>;
