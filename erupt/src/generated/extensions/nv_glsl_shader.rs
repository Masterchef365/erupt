#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const NV_GLSL_SHADER_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const NV_GLSL_SHADER_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_NV_glsl_shader");
