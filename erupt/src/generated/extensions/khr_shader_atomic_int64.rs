#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SHADER_ATOMIC_INT64_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SHADER_ATOMIC_INT64_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_shader_atomic_int64");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderAtomicInt64FeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceShaderAtomicInt64FeaturesKHR =
    crate::vk1_2::PhysicalDeviceShaderAtomicInt64Features;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderAtomicInt64FeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceShaderAtomicInt64FeaturesKHRBuilder<'a> =
    crate::vk1_2::PhysicalDeviceShaderAtomicInt64FeaturesBuilder<'a>;
