#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const AMD_MIXED_ATTACHMENT_SAMPLES_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const AMD_MIXED_ATTACHMENT_SAMPLES_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_AMD_mixed_attachment_samples");
