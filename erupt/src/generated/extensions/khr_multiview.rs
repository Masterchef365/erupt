#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_MULTIVIEW_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_MULTIVIEW_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_multiview");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMultiviewFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceMultiviewFeaturesKHR = crate::vk1_1::PhysicalDeviceMultiviewFeatures;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMultiviewFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceMultiviewFeaturesKHRBuilder<'a> =
    crate::vk1_1::PhysicalDeviceMultiviewFeaturesBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMultiviewPropertiesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceMultiviewPropertiesKHR = crate::vk1_1::PhysicalDeviceMultiviewProperties;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMultiviewPropertiesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceMultiviewPropertiesKHRBuilder<'a> =
    crate::vk1_1::PhysicalDeviceMultiviewPropertiesBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassMultiviewCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type RenderPassMultiviewCreateInfoKHR = crate::vk1_1::RenderPassMultiviewCreateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassMultiviewCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type RenderPassMultiviewCreateInfoKHRBuilder<'a> =
    crate::vk1_1::RenderPassMultiviewCreateInfoBuilder<'a>;
