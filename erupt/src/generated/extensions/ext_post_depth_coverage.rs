#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_POST_DEPTH_COVERAGE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_POST_DEPTH_COVERAGE_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_EXT_post_depth_coverage");
