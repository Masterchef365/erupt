#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_RELAXED_BLOCK_LAYOUT_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_RELAXED_BLOCK_LAYOUT_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_relaxed_block_layout");
