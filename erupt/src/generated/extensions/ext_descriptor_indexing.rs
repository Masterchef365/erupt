#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_DESCRIPTOR_INDEXING_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_EXT_descriptor_indexing");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDescriptorBindingFlagsEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DescriptorBindingFlagsEXT = crate::vk1_2::DescriptorBindingFlags;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDescriptorBindingFlagBitsEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DescriptorBindingFlagBitsEXT = crate::vk1_2::DescriptorBindingFlagBits;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDescriptorIndexingFeaturesEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceDescriptorIndexingFeaturesEXT =
    crate::vk1_2::PhysicalDeviceDescriptorIndexingFeatures;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDescriptorIndexingFeaturesEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceDescriptorIndexingFeaturesEXTBuilder<'a> =
    crate::vk1_2::PhysicalDeviceDescriptorIndexingFeaturesBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDescriptorIndexingPropertiesEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceDescriptorIndexingPropertiesEXT =
    crate::vk1_2::PhysicalDeviceDescriptorIndexingProperties;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDescriptorIndexingPropertiesEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceDescriptorIndexingPropertiesEXTBuilder<'a> =
    crate::vk1_2::PhysicalDeviceDescriptorIndexingPropertiesBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDescriptorSetLayoutBindingFlagsCreateInfoEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DescriptorSetLayoutBindingFlagsCreateInfoEXT =
    crate::vk1_2::DescriptorSetLayoutBindingFlagsCreateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDescriptorSetLayoutBindingFlagsCreateInfoEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DescriptorSetLayoutBindingFlagsCreateInfoEXTBuilder<'a> =
    crate::vk1_2::DescriptorSetLayoutBindingFlagsCreateInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDescriptorSetVariableDescriptorCountAllocateInfoEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DescriptorSetVariableDescriptorCountAllocateInfoEXT =
    crate::vk1_2::DescriptorSetVariableDescriptorCountAllocateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDescriptorSetVariableDescriptorCountAllocateInfoEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DescriptorSetVariableDescriptorCountAllocateInfoEXTBuilder<'a> =
    crate::vk1_2::DescriptorSetVariableDescriptorCountAllocateInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDescriptorSetVariableDescriptorCountLayoutSupportEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DescriptorSetVariableDescriptorCountLayoutSupportEXT =
    crate::vk1_2::DescriptorSetVariableDescriptorCountLayoutSupport;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDescriptorSetVariableDescriptorCountLayoutSupportEXT.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DescriptorSetVariableDescriptorCountLayoutSupportEXTBuilder<'a> =
    crate::vk1_2::DescriptorSetVariableDescriptorCountLayoutSupportBuilder<'a>;
