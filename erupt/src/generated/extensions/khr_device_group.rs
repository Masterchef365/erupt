#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_DEVICE_GROUP_SPEC_VERSION: u32 = 4;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_DEVICE_GROUP_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_device_group");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_GET_DEVICE_GROUP_PEER_MEMORY_FEATURES_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkGetDeviceGroupPeerMemoryFeaturesKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CMD_SET_DEVICE_MASK_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkCmdSetDeviceMaskKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CMD_DISPATCH_BASE_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkCmdDispatchBaseKHR");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPeerMemoryFeatureFlagsKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PeerMemoryFeatureFlagsKHR = crate::vk1_1::PeerMemoryFeatureFlags;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryAllocateFlagsKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type MemoryAllocateFlagsKHR = crate::vk1_1::MemoryAllocateFlags;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPeerMemoryFeatureFlagBitsKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PeerMemoryFeatureFlagBitsKHR = crate::vk1_1::PeerMemoryFeatureFlagBits;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryAllocateFlagBitsKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type MemoryAllocateFlagBitsKHR = crate::vk1_1::MemoryAllocateFlagBits;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryAllocateFlagsInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type MemoryAllocateFlagsInfoKHR = crate::vk1_1::MemoryAllocateFlagsInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryAllocateFlagsInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type MemoryAllocateFlagsInfoKHRBuilder<'a> = crate::vk1_1::MemoryAllocateFlagsInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindBufferMemoryDeviceGroupInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BindBufferMemoryDeviceGroupInfoKHR = crate::vk1_1::BindBufferMemoryDeviceGroupInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindBufferMemoryDeviceGroupInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BindBufferMemoryDeviceGroupInfoKHRBuilder<'a> =
    crate::vk1_1::BindBufferMemoryDeviceGroupInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindImageMemoryDeviceGroupInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BindImageMemoryDeviceGroupInfoKHR = crate::vk1_1::BindImageMemoryDeviceGroupInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindImageMemoryDeviceGroupInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BindImageMemoryDeviceGroupInfoKHRBuilder<'a> =
    crate::vk1_1::BindImageMemoryDeviceGroupInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceGroupRenderPassBeginInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DeviceGroupRenderPassBeginInfoKHR = crate::vk1_1::DeviceGroupRenderPassBeginInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceGroupRenderPassBeginInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DeviceGroupRenderPassBeginInfoKHRBuilder<'a> =
    crate::vk1_1::DeviceGroupRenderPassBeginInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceGroupCommandBufferBeginInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DeviceGroupCommandBufferBeginInfoKHR = crate::vk1_1::DeviceGroupCommandBufferBeginInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceGroupCommandBufferBeginInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DeviceGroupCommandBufferBeginInfoKHRBuilder<'a> =
    crate::vk1_1::DeviceGroupCommandBufferBeginInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceGroupSubmitInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DeviceGroupSubmitInfoKHR = crate::vk1_1::DeviceGroupSubmitInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceGroupSubmitInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DeviceGroupSubmitInfoKHRBuilder<'a> = crate::vk1_1::DeviceGroupSubmitInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceGroupBindSparseInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DeviceGroupBindSparseInfoKHR = crate::vk1_1::DeviceGroupBindSparseInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceGroupBindSparseInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type DeviceGroupBindSparseInfoKHRBuilder<'a> =
    crate::vk1_1::DeviceGroupBindSparseInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDeviceGroupPeerMemoryFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetDeviceGroupPeerMemoryFeaturesKHR =
    crate::vk1_1::PFN_vkGetDeviceGroupPeerMemoryFeatures;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetDeviceMaskKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetDeviceMaskKHR = crate::vk1_1::PFN_vkCmdSetDeviceMask;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdDispatchBaseKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdDispatchBaseKHR = crate::vk1_1::PFN_vkCmdDispatchBase;
#[doc = "Provided by [`extensions::khr_device_group`](extensions/khr_device_group/index.html)"]
impl crate::DeviceLoader {
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDeviceGroupPeerMemoryFeaturesKHR.html) · Function"]
    pub unsafe fn get_device_group_peer_memory_features_khr(
        &self,
        heap_index: u32,
        local_device_index: u32,
        remote_device_index: u32,
        peer_memory_features: Option<crate::vk1_1::PeerMemoryFeatureFlags>,
    ) -> crate::vk1_1::PeerMemoryFeatureFlags {
        let _function = self
            .get_device_group_peer_memory_features_khr
            .expect("`get_device_group_peer_memory_features_khr` is not loaded");
        let mut peer_memory_features = match peer_memory_features {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(
            self.handle,
            heap_index as _,
            local_device_index as _,
            remote_device_index as _,
            &mut peer_memory_features,
        );
        peer_memory_features
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetDeviceMaskKHR.html) · Function"]
    pub unsafe fn cmd_set_device_mask_khr(
        &self,
        command_buffer: crate::vk1_0::CommandBuffer,
        device_mask: u32,
    ) -> () {
        let _function = self
            .cmd_set_device_mask_khr
            .expect("`cmd_set_device_mask_khr` is not loaded");
        let _return = _function(command_buffer as _, device_mask as _);
        ()
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdDispatchBaseKHR.html) · Function"]
    pub unsafe fn cmd_dispatch_base_khr(
        &self,
        command_buffer: crate::vk1_0::CommandBuffer,
        base_group_x: u32,
        base_group_y: u32,
        base_group_z: u32,
        group_count_x: u32,
        group_count_y: u32,
        group_count_z: u32,
    ) -> () {
        let _function = self
            .cmd_dispatch_base_khr
            .expect("`cmd_dispatch_base_khr` is not loaded");
        let _return = _function(
            command_buffer as _,
            base_group_x as _,
            base_group_y as _,
            base_group_z as _,
            group_count_x as _,
            group_count_y as _,
            group_count_z as _,
        );
        ()
    }
}
