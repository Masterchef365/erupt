#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_DEPTH_RANGE_UNRESTRICTED_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const EXT_DEPTH_RANGE_UNRESTRICTED_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_EXT_depth_range_unrestricted");
