#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SHADER_SUBGROUP_EXTENDED_TYPES_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SHADER_SUBGROUP_EXTENDED_TYPES_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_shader_subgroup_extended_types");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderSubgroupExtendedTypesFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceShaderSubgroupExtendedTypesFeaturesKHR =
    crate::vk1_2::PhysicalDeviceShaderSubgroupExtendedTypesFeatures;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderSubgroupExtendedTypesFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceShaderSubgroupExtendedTypesFeaturesKHRBuilder<'a> =
    crate::vk1_2::PhysicalDeviceShaderSubgroupExtendedTypesFeaturesBuilder<'a>;
