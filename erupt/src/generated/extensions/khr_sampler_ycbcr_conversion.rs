#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SAMPLER_YCBCR_CONVERSION_SPEC_VERSION: u32 = 14;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const KHR_SAMPLER_YCBCR_CONVERSION_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_KHR_sampler_ycbcr_conversion");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_CREATE_SAMPLER_YCBCR_CONVERSION_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkCreateSamplerYcbcrConversionKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const FN_DESTROY_SAMPLER_YCBCR_CONVERSION_KHR: *const std::os::raw::c_char =
    crate::cstr!("vkDestroySamplerYcbcrConversionKHR");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSamplerYcbcrConversionKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SamplerYcbcrConversionKHR = crate::vk1_1::SamplerYcbcrConversion;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSamplerYcbcrModelConversionKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SamplerYcbcrModelConversionKHR = crate::vk1_1::SamplerYcbcrModelConversion;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSamplerYcbcrRangeKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SamplerYcbcrRangeKHR = crate::vk1_1::SamplerYcbcrRange;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkChromaLocationKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ChromaLocationKHR = crate::vk1_1::ChromaLocation;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSamplerYcbcrConversionInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SamplerYcbcrConversionInfoKHR = crate::vk1_1::SamplerYcbcrConversionInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSamplerYcbcrConversionInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SamplerYcbcrConversionInfoKHRBuilder<'a> =
    crate::vk1_1::SamplerYcbcrConversionInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSamplerYcbcrConversionCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SamplerYcbcrConversionCreateInfoKHR = crate::vk1_1::SamplerYcbcrConversionCreateInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSamplerYcbcrConversionCreateInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SamplerYcbcrConversionCreateInfoKHRBuilder<'a> =
    crate::vk1_1::SamplerYcbcrConversionCreateInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindImagePlaneMemoryInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BindImagePlaneMemoryInfoKHR = crate::vk1_1::BindImagePlaneMemoryInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindImagePlaneMemoryInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type BindImagePlaneMemoryInfoKHRBuilder<'a> = crate::vk1_1::BindImagePlaneMemoryInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImagePlaneMemoryRequirementsInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ImagePlaneMemoryRequirementsInfoKHR = crate::vk1_1::ImagePlaneMemoryRequirementsInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImagePlaneMemoryRequirementsInfoKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type ImagePlaneMemoryRequirementsInfoKHRBuilder<'a> =
    crate::vk1_1::ImagePlaneMemoryRequirementsInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceSamplerYcbcrConversionFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceSamplerYcbcrConversionFeaturesKHR =
    crate::vk1_1::PhysicalDeviceSamplerYcbcrConversionFeatures;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceSamplerYcbcrConversionFeaturesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceSamplerYcbcrConversionFeaturesKHRBuilder<'a> =
    crate::vk1_1::PhysicalDeviceSamplerYcbcrConversionFeaturesBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSamplerYcbcrConversionImageFormatPropertiesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SamplerYcbcrConversionImageFormatPropertiesKHR =
    crate::vk1_1::SamplerYcbcrConversionImageFormatProperties;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSamplerYcbcrConversionImageFormatPropertiesKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type SamplerYcbcrConversionImageFormatPropertiesKHRBuilder<'a> =
    crate::vk1_1::SamplerYcbcrConversionImageFormatPropertiesBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateSamplerYcbcrConversionKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateSamplerYcbcrConversionKHR = crate::vk1_1::PFN_vkCreateSamplerYcbcrConversion;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroySamplerYcbcrConversionKHR.html) · Alias"]
#[allow(non_camel_case_types)]
pub type PFN_vkDestroySamplerYcbcrConversionKHR = crate::vk1_1::PFN_vkDestroySamplerYcbcrConversion;
#[doc = "Provided by [`extensions::khr_sampler_ycbcr_conversion`](extensions/khr_sampler_ycbcr_conversion/index.html)"]
impl crate::DeviceLoader {
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateSamplerYcbcrConversionKHR.html) · Function"]
    pub unsafe fn create_sampler_ycbcr_conversion_khr(
        &self,
        create_info: &crate::vk1_1::SamplerYcbcrConversionCreateInfo,
        allocator: Option<&crate::vk1_0::AllocationCallbacks>,
        ycbcr_conversion: Option<crate::vk1_1::SamplerYcbcrConversion>,
    ) -> crate::utils::VulkanResult<crate::vk1_1::SamplerYcbcrConversion> {
        let _function = self
            .create_sampler_ycbcr_conversion_khr
            .expect("`create_sampler_ycbcr_conversion_khr` is not loaded");
        let mut ycbcr_conversion = match ycbcr_conversion {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut ycbcr_conversion,
        );
        crate::utils::VulkanResult::new(_return, ycbcr_conversion)
    }
    #[inline]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroySamplerYcbcrConversionKHR.html) · Function"]
    pub unsafe fn destroy_sampler_ycbcr_conversion_khr(
        &self,
        ycbcr_conversion: Option<crate::vk1_1::SamplerYcbcrConversion>,
        allocator: Option<&crate::vk1_0::AllocationCallbacks>,
    ) -> () {
        let _function = self
            .destroy_sampler_ycbcr_conversion_khr
            .expect("`destroy_sampler_ycbcr_conversion_khr` is not loaded");
        let _return = _function(
            self.handle,
            match ycbcr_conversion {
                Some(v) => v,
                None => Default::default(),
            },
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
        );
        ()
    }
}
