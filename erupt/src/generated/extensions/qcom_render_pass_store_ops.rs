#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const QCOM_RENDER_PASS_STORE_OPS_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
pub const QCOM_RENDER_PASS_STORE_OPS_EXTENSION_NAME: *const std::os::raw::c_char =
    crate::cstr!("VK_QCOM_render_pass_store_ops");
